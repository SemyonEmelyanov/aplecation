package com.example.android_navigation_fragment_restapi

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface jsonApi {
    //https://api.icndb.com/jokes/random/6
    @GET("jokes")
    fun getInfo(): Call<model>

    @GET("jokes/random/{id}")
    fun getInfoRandom(
        @Path("id") id: String
    ): Call<model>
}