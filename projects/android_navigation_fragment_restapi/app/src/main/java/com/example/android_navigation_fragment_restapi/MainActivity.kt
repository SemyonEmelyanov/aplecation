package com.example.android_navigation_fragment_restapi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import okhttp3.OkHttpClient
import okhttp3.Request
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException

class MainActivity : AppCompatActivity() {
    private val list = mutableListOf<String>("Cat","Joke","Ter")
    private lateinit var mRcView: RecyclerView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://api.icndb.com/")
            .build()
        val json = retrofit.create(jsonApi::class.java)
        val call: Call<model> = json.getInfo()
        call.enqueue(object: Callback<model> {
            override fun onResponse(
                call: Call<model>,
                response: Response<model>
            ) {
                val test = response.body()
                Log.i("TAG", test!!.value[10].joke)

            }
            override fun onFailure(call: Call<model>, t: Throwable) {
                Log.e("ErrorService", "Error Service = "+t.message)
            }
        })
        mRcView = findViewById(R.id.container)
        mRcView.layoutManager = LinearLayoutManager(this)
        mRcView.adapter = CustomRecyclerAdapter(list)



//        val request = Request.Builder().url("https://api.icndb.com/jokes").get().build()
//        OkHttpClient().newCall(request).enqueue(object : okhttp3.Callback{
//            override fun onFailure(call: okhttp3.Call, e: IOException) {
//                Log.e("ErrorService", "Error Service = "+e.message)
//            }
//
//            override fun onResponse(call: okhttp3.Call, response: okhttp3.Response) {
//                if (response.code == 200){
//                    Log.i("TAG","Yes: " + response.body.toString())
//                    val json = JSONObject(response.body!!.string())
//                    Log.i("ServiceApi", json.getString("value").get(0).toString())
//
//                }
//                else {
//                    Log.i("TAG","No")
//                }
//            }
//
//        })



        /* LayoutManager:
        LinearLayoutManager - дочерние элементы располагаются верт. или гориз
        GridLayoutManager - в сетке, GridView
        StaggeredGridLayoutManager - неравномерная сетка
        ..................
        ItemDecortion - внешний вид
        ItemAnimator - отвечает за анимации
        Adapter - отвечает за логику

        ViewHolder - является одним из основных компонентов создания view элемента RV
        */


    }
}