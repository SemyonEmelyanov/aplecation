package com.example.android_navigation_fragment_restapi

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView

class CustomRecyclerAdapter (var list: MutableList<String>) : RecyclerView.Adapter<CustomRecyclerAdapter.ViewHolder>() {
//    var list: MutableList<String> = mutableListOf()
    class ViewHolder (itemView: View): RecyclerView.ViewHolder(itemView)  {
        var  text_joke: TextView? = null
        
        init {
            text_joke = itemView.findViewById(R.id.element_text)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.element,parent,false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.text_joke!!.text = list[position]
    }

    override fun getItemCount(): Int {
        return list.size
    }

}