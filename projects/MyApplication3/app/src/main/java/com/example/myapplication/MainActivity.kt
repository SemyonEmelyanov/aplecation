package com.example.myapplication

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private lateinit var  button_1: Button
    private lateinit var text_view: TextView

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        text_view = findViewById(R.id.textView)
    }

    private val arrayOperationAndPoint = listOf("-","+","*", "/",".")
    private val arrayOperationHigh = listOf("*", "/")
    private val arrayOperationLaw = listOf("+", "-")
    private val TAG = "MainActivity"

    // функция, которую мы присвоили для View элемента Button в поле onClic, функция отвечает за обработку нажатий.
    @SuppressLint("SetTextI18n")
    fun onClic(v: View){
        when(v){
            is Button -> {
                when(v.text){
                    // обработка знаков операция, мы не можем ставить рядом знаки операци,т.е. подряд, и не можем ставить знаки операции рядом с точками
                    "+" -> {
                        if (v.text.isNotEmpty()  && !arrayOperationAndPoint.contains((text_view.text.toString()[text_view.text.length -1]).toString()) ) text_view.setText(text_view.text.toString() + v.text.toString())
                    }
                    "-" -> {
                        if (v.text.isNotEmpty()  && !arrayOperationAndPoint.contains((text_view.text.toString()[text_view.text.length -1]).toString()) ) text_view.setText(text_view.text.toString() + v.text.toString())
                    }
                    "*" -> {
                        if (v.text.isNotEmpty() && !arrayOperationAndPoint.contains((text_view.text.toString()[text_view.text.length -1]).toString()) ) text_view.setText(text_view.text.toString() + v.text.toString())
                    }
                    "/" -> {
                        if (v.text.isNotEmpty()  && !arrayOperationAndPoint.contains((text_view.text.toString()[text_view.text.length -1]).toString()) ) text_view.setText(text_view.text.toString() + v.text.toString())
                    }
                    "C" ->{ // удаление символа
                        if (text_view.text.isNotEmpty()){
                            val textClear = text_view.text.toString().substring(0,text_view.text.length -1)
                            text_view.text = textClear
                        }
                    } // очистка всего поля
                    "CLEAR" -> {
                        text_view.text = ""
                    }
                    "." -> { // обрабатываем точку, мы не можем её потсавить вместе ещё с одной точкой и знаком операции
                        if (text_view.text.isNotEmpty() &&  !arrayOperationAndPoint.contains((text_view.text.toString()[text_view.text.length -1]).toString())) text_view.setText(text_view.text.toString() + v.text.toString())

                    }
                    "="->{ // обрабатываем, если у нас ничего не введено, то логика расчёта не будет вызываться.
                        if (text_view.text.isNotEmpty())
                            text_view.text = result()
                    }
                    else -> { // добавляем цифры в наше выражение
                        text_view.setText(text_view.text.toString()+v.text.toString())
                    }
                }
            }
            else -> true
        }
    }
    // основная наша функция для расчёта результата, в ней мы вызываем вспомогательный функции
    fun result():String{
        var list = createListValue()
        if (list.isEmpty()) text_view.text = "Error!"
        var listPreview = createListPreview(list)
        if (listPreview.isEmpty()) text_view.text = "Error!"
        var result = resultValue(listPreview)
        return result
    }

    private var flagErrorDevideZero = false

    // функция, которая считает нам последний писок, огда в нём осталис только цифры и знаки операции + -, на выходе получаем число, которое преобразуем в String.
    fun resultValue(list: MutableList<String>): String{
        var result = list[0].toFloat()
        var i = 1
        while(i < list.size){
            when(list[i]){
                "+" ->{
                    result += list[i+1].toFloat()
                    i+=2
                }
                "-" ->{
                    result -= list[i+1].toFloat()
                    i+=2
                }
                else -> {
                }
            }

        }
        return result.toString()
    }

    // создаём по нашей строке список из чисел и знаков операций
    fun createListValue():MutableList<String>{
        val list = mutableListOf<String>()
        var currentValue = ""
        for (i in 0..text_view.text.toString().length-1){
            val symbol = text_view.text.toString()[i]
            if(symbol.isDigit() || symbol.toString() == "."){
                currentValue+= symbol.toString()
            }
            else{
                list.add(currentValue)
                currentValue = ""
                list.add(symbol.toString())
            }
        }
        list.add(currentValue)
        return list
    }
    // функция,которая возвращает нам список без приоритетных знаков операций, а именно * /, в ней мы вызываем функцию, которая отвечает за исключение из списка этих знаков.
    fun createListPreview(list: MutableList<String>): MutableList<String>{

        var result = list

        if(result.contains("*") || result.contains("/")){
            result = createListLaw(result) ?:  arrayListOf()
        }
        return result
    }

    // функция, оторая исключает из списка операции *, /.
    fun createListLaw(list:MutableList<String> ): MutableList<String>? {
        var i = 1
        while (i < list.size){
            if (arrayOperationHigh.contains(list[i]) ){
                val operation = list[i]
                val prefix = list[i-1].toFloat()
                val postfix = list[i+1].toFloat()
                when (operation) {
                    "*" -> {
                        list.removeAt(i)
                        list.removeAt(i)
                        list[i-1] = (prefix * postfix).toString()
                    }
                    "/" -> {
                        if (postfix == 0F) {
                            text_view.text = "Devide on zero"
                            return null
                        } else {
                            list.removeAt(i)
                            list.removeAt(i)
                            list[i-1] = (prefix / postfix).toString()
                        }
                    }
                    else -> {
                        Log.e(
                            TAG,
                            "Error! prefix = $prefix operation = $operation postfix = $postfix"
                        )
                    }

                }
            }
            else if(arrayOperationLaw.contains(list[i])){
                i+=2
            }
        }
        return list
    }


    /// 2+3+4   ["2","+"..]


}